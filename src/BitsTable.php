<?php

namespace Bits;

class Bits_Admin_Table {
	private $name;
	
	function __construct($name) {
		$this->name = $name;
	}

	function getName() {
		return $this->name;
	}
}